from random import*
import time

#_______________________FONCTIONS_______________________

#-----------------------verif_mdp-----------------------
def verif_mdp (mdp):
    """entree:'str' le mot de passe
    sortie:'int' l'id de la reponse"""
    if incorrect(mdp)==True:
        return rep8        
    else:
        if nb_maj(mdp)==False:
            return rep2
        elif nb_minu(mdp)==False:
            return rep3
        elif nb_chiffre(mdp)==False:
            return rep4
        elif arbitre(mdp)==False:
            return rep5
        elif som_mdp(mdp)==False:
            return rep6
        elif prem_lettre(mdp)==False:
            return rep7
        else:
            return rep9

"""pour les 7 fonctions suivantes:
    entrée:'int' le mot de passe
    sortie: 'bool' renvoie si le mot de passe rempli certaine contitions"""
def incorrect(mdp):
    return mdp=="incorrect"    

def nb_maj(mdp):
    i=0
    nb=0
    while i< len(mdp):
        if ord(mdp[i])>=65 and ord(mdp[i])<=90: #ord sert a avoir l'equivalent de la valeu en ascii
            nb+=1
        i+=1
    return nb==8
    
def nb_minu(mdp):
    i=0
    nb=0
    while i< len(mdp):
        if ord(mdp[i])>=97 and ord(mdp[i])<=122:
            nb+=1
        i+=1
    return nb==12
        
def nb_chiffre(mdp):
    i=0
    nb=0
    while i< len(mdp):
        if ord(mdp[i])>=48 and ord(mdp[i])<=57:
            nb+=1
        i+=1
    return nb==13

def arbitre (mdp):
    i=0
    for i in range( len(mdp)):
        if mdp[i] == "a" or mdp[i] == "A" and i<len(mdp)-1:
            i+=1
            if mdp[i] == "r" or mdp[i] == "R" and i<len(mdp)-1:
                i+=1
                if mdp[i] == "b" or mdp[i] == "B" and i<len(mdp)-1:
                    i+=1
                    if mdp[i] == "i" or mdp[i] == "I" and i<len(mdp)-1:
                        i+=1
                        if mdp[i] == "t" or mdp[i] == "T" and i<len(mdp)-1:
                            i+=1
                            if mdp[i] == "r" or mdp[i] == "R" and i<len(mdp)-1:
                                i+=1
                                if mdp[i] == "e" or mdp[i] == "E" and i<len(mdp)-1:
                                    return True
    return False
    
def som_mdp (mdp):
    somme=0
    for i in range(len(mdp)):
        if ord(mdp[i])>=48 and ord(mdp[i])<=57:
            somme+=int(mdp[i])
    return somme==50

def prem_lettre(mdp):
    alphabet="abcdefghij"
    alphabet_maj="ABCDEFGHIJ"
    for i in range(len(alphabet)):
        if alphabet[i]!= mdp[i] and alphabet_maj[i] != mdp[i]:
            return False
    return True

#-----------------------créationdossier-----------------------

def creationdossier():
    """
    Fonction demandant un certain nombre de questions et retournant toutes les réponses sous forme de chaînes de caractère.
    """
    return(input("Quel est vrotre nom de famille? \n"),input("Votre prénom ? \n"),input("Le nom de votre père ? \n"),input("Le prénom de votre soeur ? \n"),input("Le deuxièmre prénom de votre mère ? \n"),input("Le nom de jeune fille de votre grande-tante maternelle la plus âgée ? \n"),input("Le nom du chien de votre oncle ? \n"),input("Le nom de la rue d'habitation du fils caché de votre arrière grand-père paternel ? \n"),input("Votre numéro de sécurité sociale ? \n"),input("Combien avez-vous d'animaux chez vous ? \n"),input("Combien y a-t-il d'ampoules dans l'ensemble de vos propriétés immobilières ? \n"),input("Votre groupe sanguin ? \n"),input("Votre dernier repas de Noël ? \n"),input("Votre pointure ? \n"),input("Votre couleur préférée, à part orange, violet et jaune ? \n"))

#-----------------------liste formation-----------------------
def nombres_aléatoires():
    """
    sortie : 3 listes de nombres et de formules
    crée trois listes, la première avec des nombres normaux, et la deuxième et troisième avec des formules ++
    """
    liste1 = []
    liste2 = []
    liste3 = []
    a = 0
    for i in range(100):
        a = a + 1
        liste1.append(str(a))
    for i in range(50):
        a = a + 1
        liste2.append(str(a))
        liste3.append(str(a))
    for i in range(10):
        liste2.append("√"+str(randint(2,40)))
    for i in range(10):
        liste2.append(str(randint(2,10))+"x²+"+str(randint(2,10))+"x+"+str(randint(2,20)))
    for i in range(50):
        liste3.append("√"+str(randint(2,40)))
    for i in range(50):
        liste3.append(str(randint(2,10))+"x²+"+str(randint(2,10))+"x+"+str(randint(2,20)))
        
    return(liste1,liste2,liste3)

def FormationsAleatoires(type_formation,motaleatoire1,motaleatoire2,ville):
    
    formation = type_formation[randint(0,len(type_formation)-1)] +" "+ motaleatoire1[randint(0,len(motaleatoire1)-1)] +"-"+ motaleatoire2[randint(0,len(motaleatoire2)-1)] +" sur "+ ville[randint(0,len(ville)-1)] + " Taux d'acceptation: " + str(randint(-20,120)) + "%"
    return formation

def liste_formations(liste_1,liste_2,liste_3,liste_4,liste_5,liste_6,liste_7):
    formation = []
    for i in range(100) :
        formation.append(FormationsAleatoires(liste_1,liste_2,liste_3,liste_4)) 
    for i in range(100) :
        formation.append(FormationsAleatoires(liste_1,liste_2,liste_3,liste_5))
    for i in range(100) :
        formation.append(FormationsAleatoires(liste_1,liste_2,liste_3,liste_5))
        
    for i in range(len(formation)):
        print(formation[i] + "\n")

    return formation

def appartient_lst (mots,lst):
    for y in range (len(lst)):
        if lst[y][0]==" ":
            lst[y]=lst[y][1:]
        if lst[y]==mots:
            return mots
    return False

#-----------------------liste formation-----------------------
def file_attente ():
    """pas d'entree
    pas de sortie
    spécification: affiche des message et des demandes dans la console"""
    print("Nous sommes témoins d'une affluence considérable de candidats aspirant à accéder à cette prestigieuse formation. Dans notre quête de sélection des esprits les plus brillants en mathématiques, nous vous prions d'estimer le nombre de prétendants vous précédents dans cette file d'attente.")
    #calcule dont la rep est 21
    question="(5*1.4x)/2 = 10.5-(28y)/8 où x et y ",chr(8712),chr(8469), "\{0} et x > y ; x est le nombre des dixaines et y le nombre des unitées "
    rep=int(input(question))
    while rep != 21:
        rep=int(input("Ce n'est pas la bonne reponse, recommencez "))
    print("Bravo ! Il reste bien 21 candidats avant vous dans la file d'attente, veuillez patienter 30 secondes")
    for i in range(31):
        time.sleep(1)
        print(30-i)

#-----------------------fonction redirection-----------------------
def qcm():
    """pas d'entree
    pas de sortie
    spécification: affiche des message et des demandes dans la console"""
    score = 0
    tabl_question=["Quel est le salaire moyen en Bolivie ? \n Réponse A : 362,34€ \n Réponse B : 143,383.28$ ZWD \n Réponse C : 2,722.83 Bolivianos \n Réponse D : 58,602 Y Japonais\n",\
                "Quel est l'ingrédient principal du gâteau au yaourt ? \n Réponse A : Le yaourt \n Réponse B : Le moule à gâteau \n Réponse C : La farine \n Réponse D : L'uranium\n",\
                "Combien y’a t-il d'épisodes dans les feux de l’amour ? \n Réponse A : 12 770 \n Réponse B : 12 \n Réponse C : Un certain nombre \n Réponse D : Beaucoup trop\n",\
                "Combien de dents a le phacochère ? \n Réponse A : 34 \n Réponse B : 8 000 000 \n Réponse C : 2 \n Réponse D : il n'en a pas\n",\
                "En quel année est né votre père ? \n Réponse A : En bretagne \n Réponse B : 1924 \n Réponse C : 2017 \n Réponse D : 0\n",\
                "Qui a gagné le combat du 07/03/2024 entre Doumbé et Baki ? \n Réponse A : L'arbitre \n Réponse B : Doumbé \n Réponse C : Nadine Morano \n Réponse D : Obi-wan Kenobi\n"]

    for i in range(len(tabl_question)):
        reponse=input(tabl_question[i])
        if reponse == "a" :
            score += 1
        elif reponse == "b" :
            score += 2
        elif reponse == "c" :
            score += 3
        elif reponse == "d" :
            score += 4
        else :
            score += 0
    print("Selon vos réponse, voici un commentaire a votre sujet que nous proposons: ")
    if score < 6 :
        print("Retournez à l'école.")
    elif score == 6 or score == 12 or score == 18 or score == 24 :
        print("Je crois que votre clavier à un problème.")
    elif score == 7 :
        print("Misez votre prêt étudiant à la lotterie")
    elif score == 8 :
        print("Niet Slecht")
    elif score == 9 :
        print("Vous feriez un sans-abris d'exception.")
    elif score == 10 :
        print("Vous devriez vous mettre au football.")
    elif score == 11 :
        print("Vous avez la force mentale d'un écureuil. Continuez ainsi.")
    elif score == 13 :
        print("Des vrais réponses de Gémeau.")
    elif score == 14 :
        print("Abandonnez.")
    elif score == 15 :
        print("Appelez une ambulance en urgence.")
    elif score == 16 :
        print("Envisagez d'aller élever des chèvres dans le Ventoux.")
    elif score == 17 :
        print("Vous pourriez vous engager dans l'armée de terre.")
    elif score == 19 :
        print("Devenez tanathopracteur.")
    elif score == 20 :
        print("Tentez une licence IV, à défaut d'une licence intellectuelle.")
    elif score == 21 :
        print("Prenez congé avec la vie.")
    elif score == 22 :
        print("Sacrifiez un rat trentenaire à la demi-lune, peut-être qu'une secte sera créée en votre nom.")
    elif score == 23 :
        print("Vous êtes un panda.")

#-----------------------fonction lettre phase 2-----------------------
def phase2():
    """pas d'entree
    sortie: un booleen qui return true si l'utilisateur est allé au bout de la 
    fonction et false sinon pour pouvoir faire une boucle tant que l'utilisateur
    n'a pas entré ce qu'il doit entrer"""
    consigne = input(
        "voulez vous connaitre les conditions de redactions de votre lettre de motivation ")
    while (consigne != "oui" and consigne != "non"):
        consigne = input("veuillez repondre par oui ou non ")
    if consigne == "oui":
        print("\nVotre lettre doit comporter au minimun 15000 charactères ")
        print("Votre lettre doit faire un resumer de tout les metiers que vous avez voulus faire dans votre vie ")
        print("expliquez votre arbre genealogique afin d'être sur qu'il est pure ")
        print("Quel etait le nom de votre prof de math en 6eme ")
        print(
            "expliquez pourquoi dora l'explorattrice est un dessin anime tres constructif ")
        print("Pourquoi un corbeau ? ")

    elif (consigne == "non"):
        print("Dommage pour vous , bonne chanche pour votre rédaction .")
    print("")
    lettre = input("Veuillez maintenant rédiger votre lettre en prenant soin de respecter les consignes .Si vous etes désesperer tapper 'desesperer' en toute lettre ")
    while lettre != "je suis motivé":
        if lettre == "desesperer":
            print("\n Vous devez juste expliquer que vous êtes motivé\nNous serons en capacité de vous aider dans deux siecles environs\n")
            return False
        else:
            lettre=input("expliquez que vous etes motivé. Si vous etes désesperer tapper 'desesperer' en toute lettre ")
    print("bravo vous pouvez passer a l'étape suivante")
    return True

#-----------------------fonction timer-----------------------
def timer(seconds):
    a = 0
    for i in range(seconds):
        time.sleep(1)
        a = a + 1
        print(a)

#--------------------------fonction crash---------------------------
def crash(proba):
    """entrée: 'int' la probabilité que le code crash (volontairement)
    sortie: 'bool' true si le code crash, false sinon"""
    nombre = randint(1,proba)
    if nombre == 1:
        print("Erreur, trop d'utilisateurs redirection vers le site, veuillez patienter")
        time.sleep(3)
        print("...")
        time.sleep(4)
        return False
    return True
#_______________________VARIABLES POUR LE CODE_______________________

#-----------------------fonction verif_mdp-----------------------
rep1="Votre mot de passe est incorrect. "
rep2="Votre mot de passe est incorrect. Votre mot de passe doit contenir 8 majuscule."
rep3="Votre mot de passe est incorrect. Votre mot de passe doit contenir 12 minuscule."
rep4="Votre mot de passe est incorrect. Votre mot de passe doit contenir 13 chiffre."
rep5="Votre mot de passe est incorrect. Votre mot de passe doit contenir le mot arbitre."
rep6="Votre mot de passe est incorrect. La somme des chiffre de votre mot de passe doit être égal à 50."
rep7="Votre mot de passe est incorrect. Le debut de votre mot de passe doit contenir les 10 première lettre de l’alphabet dans l’ordre."
rep8="Bravo votre mdp est correct !"
rep9="Votre mot de passe est certe securisé mais le mot de passe est incorrect. "

#-----------------------fonction liste_formation-----------------------
liste_1 = ["CAP","BEP","BUT","BTS", "DUT", "Baccalauréat Professionnel", "Licence Professionnelle", "Master", "Doctorat",]
liste_2 = ["cuisine","Ingénierie", "Médecine", "Finance", "Design", "Architecture", "Marketing", "Sciences", "Génie", "Cuisine", "Mode", "Électrique", "Comptabilité", "Sport", "Production", "Immobilier", "Informatique", "Bien-Être", "Droit", "Artisanat", "Énergie", "Communication", "Éducation", "Tourisme", "Management", "Musique", "Graphisme", "Environnement", "Transport", "Santé", "Journalisme"]
liste_3 = ["informatique","boucherie","jardinage","alimentation","architecture","paysagisme","aquaculture","arboriculture","animalier","apiculterie","arts graphiques","administration","imprimerie","hotellerie","humanitaire"]
liste_4 = ["Paris", "Marseille", "Lyon", "Toulouse", "Nice", "Nantes", "Strasbourg", "Montpellier", "Bordeaux", "Lille", "Rennes", "Reims", "Le Havre", "Saint-Étienne", "Toulon", "Grenoble", "Dijon", "Angers", "Nîmes", "Villeurbanne", "Saint-Denis", "Le Mans", "Aix-en-Provence", "Clermont-Ferrand", "Brest", "Tours", "Limoges", "Amiens", "Annecy", "Perpignan", "Boulogne-Billancourt", "Metz", "Besançon", "Orléans", "Saint-Denis", "Argenteuil", "Mulhouse", "Rouen", "Montreuil", "Caen", "Nancy", "Saint-Paul", "Tourcoing", "Roubaix", "Nanterre"]
liste_5 = ["Bitche", "La Queue-en-Brie", "Condom", "Montcuq", "Pisseleu", "Saint-Léger-en-Yvelines", "Bellegarde-en-Marche", "Pauvre", "Anus", "La Motte-Tilly", "Monteton", "La Petite-Pierre", "Buzet-sur-Tarn", "Les Arques", "Sauveterre-de-Guyenne", "Vézac", "Le Châtenet-en-Dognon", "Saint-Amant-de-Boixe", "Villefranche-du-Queyran", "Brûlain", "La Roche-Blanche", "Marzy", "La Vache", "Figeac", "La Ferté-sous-Jouarre", "La Machine", "La Baffe", "Le Cendre", "Le Mans", "Puymoyen", "Montcuq-en-Quercy-Blanc", "La Chapelle-Biche", "Thugny-Trugny", "Châtenay-Malabry", "Chattes", "Thiers", "Noyant-la-Plaine", "Maransin", "Trécon", "La Croix-en-Champagne", "Le Poizat-Lalleyriat", "La Boulaye"]
liste_6 = nombres_aléatoires()[0] 
liste_7 = nombres_aléatoires()[0] + nombres_aléatoires()[1]




#_______________________MAIN_______________________
def main ():
    """Renvoie true une fois que l'utilisateur a fini le jeu afin de pouvoir le
    relancer depuis le debut si un crash (volontaire) a lieu 
    """
    print("Bienvenu sur Parcourmoins, suite à votre précedente visite sur notre site ou vous vous êtes inscrit sous le nom d'utilisateur: 'User_38966.18.5.18\nVeuillez confirmer que c'est vous en entrant votre mot de passe.\n")
    i=False
    while i==False:
        mdp=input("Entrez votre mot de passe: ")
        print(verif_mdp(mdp))
        if verif_mdp(mdp)== "Bravo votre mdp est correct !":
            i=True

    if crash(15)==False:
        return False 
           
    print("Vous allez créer votre dossier.")
    creationdossier()

    if crash(15)==False:
        return False 

    print("VOS INFORMATIONS ONT BIEN ETE PRISENT EN COMPTE, MAINTENANT VEUILLEZ CHOISIR VOS FORMATIONS:")
    print("")
    time.sleep(3.3)
    liste_des_formations=liste_formations(liste_1,liste_2,liste_3,liste_4,liste_5,liste_6,liste_7)
    bonne_formation=False
    while bonne_formation==False:
        formation=input("Choisissez une formation: ")
        bonne_formation=appartient_lst(formation,liste_des_formations)

    print("A présent, voyons si il y a de la place dans la formation ",formation," que vous avez choisis: ")
    print("")
    time.sleep(3)
    file_attente ()

    print("Au cas ou vous n'êtes pas accepté dans la formation ",formation," veuillez remplir un questionnaire qui determinera certaines de vos connaissances afin que nous ayons une idée de plan B à vous proposer\n")
    qcm()

    if crash(15)==False:
        return False

    print("Veuillez écrire une lettre de motivation qui sera transmise aux formations dont vous avez fait la demande")
    time.sleep(1.5)
    p2 = False
    while p2 != True:
        p2 = phase2()

    if crash(15)==False:
        return False

    time.sleep(1)
    print("Pacourmoins s'emploie actuellement à diligenter une recherche des alternatives les plus adaptées à vos besoins pour l'année à venir. Nous vous prions de bien vouloir patienter approximativement une minute... ")
    time.sleep(2)
    timer(60)
    print("Voici ce qui incarne la meilleur évaluation quant à ce qui, selon nos analyses approfondies, semble le plus propice à façonner votre avenir, en harmonie avec les données de votre dossier. \nhttps://www.francetravail.fr/accueil/\nNous vous exprimons notre sincère reconnaissance pour avoir choisi de bénéficier des services de Pacourmoins.")

    return True

#---code main---
jeu=False
while jeu!=True:
    jeu=main()